import React, { Component } from 'react'
import './pokedex.css'
import Pokecard from './../Pokecard/Pokecard'
import Layout from './../Layout/Layout'

export class Pokedex extends Component {

  render() {
    return (
      <Layout>
        <div className="pokedex">
          <div className="pokedex-cards">
            {this.props.pokemons.map((p) => (
              <Pokecard key={p.id} id={p.id} name={p.name} type={p.type} exp={p.exp} src={p.src} />
            ))}
          </div>
        </div>
      </Layout>
    )
  }
}

export default Pokedex