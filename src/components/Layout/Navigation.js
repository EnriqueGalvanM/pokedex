import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem

} from 'reactstrap';
import { Link } from "react-router-dom"

export default class Navigation extends Component {
    state = {
        isOpen: false
    }
    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    render() {
        return (
            <Navbar color='light' light exoand='md'>
                <NavbarBrand href='/pokemon'>Pokemons</NavbarBrand>
                <NavbarToggler onClick={this.toggle}/>
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className='ml-auto' navbar>
                        <NavItem>
                            <Link to='/pokemon'>Home</Link>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        )
            }
        }
        
