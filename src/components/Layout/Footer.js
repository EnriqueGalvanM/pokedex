import React, { Component } from 'react'

export class Footer extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-body">
                    <p className="card-text">Some more card content</p>
                </div>
                <div className="card-footer">
                    2 days ago
                </div>
            </div>
        )
    }
}

export default Footer
